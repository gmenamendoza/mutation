require 'rspec'

def easy_test(implementations, expectations)
  implementations.each do |impl|
    RSpec.describe "##{impl}" do
      expectations.each_pair do |input, expected|
        it "#{input} -> #{expected}" do
          args = input.is_a?(Array) ? input : [input]
          expect(send(impl, *args)).to eq(expected)
        end
      end
    end
  end
end

