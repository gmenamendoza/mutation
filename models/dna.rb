class DNA < Sequel::Model(:dna)
  def self.from_array(arr)
    DNA.new(
      lines: arr.join("\n"),
      mutation: has_mutation(arr),
    )
  end

  def self.has_mutation(dna)
    matrix = dna.map { |word| word.split('') }

    # horizontal
    matrix.each do |arr|
      return true if mutated?(arr)
    end

    # vertical
    matrix.transpose.each do |arr|
      return true if mutated?(arr)
    end

    # diagonal - bottom left to top right
    diagonals(matrix).each do |arr|
      return true if mutated?(arr)
    end

    # diagonal - top left to bottom right
    diagonals(matrix.map(&:reverse)).each do |arr|
      return true if mutated?(arr)
    end

    false
  end

  def self.mutated?(arr)
    count = nil
    prev = nil
    arr.each do |letter|
      if letter == prev
        count += 1
        return true if count >= 4
      else
        count = 1
      end
      prev = letter
    end

    false
  end

  def self.diagonals(matrix)
    n = matrix.size
    diags = []
    (0..(n * 2) - 2).each do |i|
      arr = []
      (0..i).each do |j|
        x = j
        y = i - j
        next if x >= n || y >= n
        arr << matrix[y][x]
      end
      diags << arr
    end
    diags
  end
end
