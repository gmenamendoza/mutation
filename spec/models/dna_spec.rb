require_relative 'spec_helper'

describe DNA do
  describe '.has_mutation' do
    it 'should return false when no mutation found' do
      assert_equal(false, DNA.has_mutation(%w[ATGCGA CAGTGC TTATTT AGACGG GCGTCA TCACTG]))
    end

    it 'should return true when horizontal mutation found' do
      assert_equal(true, DNA.has_mutation(%w[ATGCGA CAGTGC TTATGT AGAAGG CCCCTA TCACTG]))
    end

    it 'should return true when vertical mutation found' do
      assert_equal(true, DNA.has_mutation(%w[ATGCGA CAGTGC CTATTT CGACGG CCGTCA TCACTG]))
    end

    it 'should return true when bottom-left to top-right mutation found' do
      assert_equal(true, DNA.has_mutation(%w[ATGCGA CACTGC TCATTT CGACGG GCGTCA TCACTG]))
    end

    it 'should return true when top-left to bottom-right mutation found' do
      assert_equal(true, DNA.has_mutation(%w[ATGCGA CAATGC TTAATT AGACAG GCGTCA TCACTG]))
    end
  end
end
