# Migrate

migrate = lambda do |version|
  require_relative 'db'
  Sequel.extension :migration
  Sequel::Migrator.apply(DB, 'migrate', version)
end

desc 'Database-related tasks'
namespace :db do
  desc 'Migrate database to latest version'
  task :up do
    migrate.call(nil)
  end

  desc 'Migrate database all the way down'
  task :down do
    migrate.call(0)
  end
end

# Spec

spec = proc do |pattern|
  sh "#{FileUtils::RUBY} -e 'ARGV.each{|f| require f}' #{pattern}"
end

desc "Run model specs"
namespace :spec do
  task :models do
    spec.call('./spec/models/*_spec.rb')
  end
end

# Default

desc "Run all specs"
task default: [:'spec:models']
