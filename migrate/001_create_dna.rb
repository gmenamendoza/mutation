Sequel.migration do
  up do
    create_table(:dna) do
      primary_key :id
      String :lines, null: false
      TrueClass :mutation, null: false
    end
  end

  down do
    drop_table(:dna)
  end
end
