require 'roda'
require 'json'

require_relative 'models'

class MutationApp < Roda
  route do |r|
    # GET /
    r.root do
      response['Content-Type'] = 'text/plain'
      <<~HELP
        Sample requests:

        $ curl --dump-header --request POST --data '{"dna": ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]}' #{r.base_url}/mutation

        $ curl --dump-header --request #{r.base_url}/stats
      HELP
    end

    r.on 'mutation' do
      r.is do
        # POST /mutation
        r.post do
          errors = []

          begin
            json = JSON::parse(r.body.read)
            dna = json['dna']
            instance = DNA.from_array(dna).save
            errors << 'DNA does not contain a mutation.' unless instance.mutation
          rescue Exception => e
            errors << e
          end

          response['Content-Type'] = 'application/json'
          if errors.empty?
            response.status = 200
            true.to_json
          else
            response.status = 403
            { errors: errors }.to_json
          end
        end
      end
    end

    # GET /stats
    r.get 'stats' do
      count_mutations = DNA.where(mutation: true).count
      count_no_mutation = DNA.where(mutation: false).count
      ratio = count_no_mutation == 0 ? nil : count_mutations / count_no_mutation

      response['Content-Type'] = 'application/json'
      {
        count_mutations: count_mutations,
        count_no_mutation: count_no_mutation,
        ratio: ratio,
      }.to_json
    end
  end
end
