# Mutation

Detects DNA mutations via a REST API and provides submission statistics.


## Requirements

- Ruby 2.6.6 (Ideally using [Rbenv](https://github.com/rbenv/rbenv) to guarantee that your development environment matches production).
- Postgres 11
- Git


## Installation

Get the code.

```bash
$ git clone  git@gitlab.com:gmenamendoza/mutation.git
$ cd mutation
```

Get dependencies.

```bash
$ gem install bundler
$ bundle install
```

Create the database user and a database for each environment.

```bash
$ sudo -u postgres createuser mutation -P
$ sudo -u postgres createdb -O mutation mutation_test
$ sudo -u postgres createdb -O mutation mutation_development
```

Create an `.env` script based on the example to set the `DATABASE_URL` environment variable.

Create the development tables:

```bash
$ rake db:up
```

## Run

A script is provided to run locally and to automatically restart the
application when a development change is detected.

```bash
$ ./run
```

A `Procfile` for Heroku is also provided.


## Usage

Submit DNA containing a mutation.

```bash
$ curl --dump-header - --request POST --data '{"dna": ["ATAG", "CCCC", "AGTA", "GCGT"]}' http://localhost:9292/mutation
HTTP/1.1 200 OK
Content-Type: application/json
Content-Length: 4
...

true
```

Submit DNA without mutations.

```bash
$ curl --dump-header - --request POST --data '{"dna": ["ATAG", "CCCA", "AGTA", "GCGT"]}' http://localhost:9292/mutation
HTTP/1.1 403 Forbidden
Content-Type: application/json
Content-Length: 47
...

{"errors":["DNA does not contain a mutation."]}
```

Query submission statistics.

```bash
$ curl --dump-header - http://localhost:9292/stats
HTTP/1.1 200 OK
Content-Type: application/json
Content-Length: 54
...

{"count_mutations":2,"count_no_mutation":1,"ratio":2}
```


## Tests

All tests can be run as the default Rake task.

```bash
$ rake
$ rake spec:models
```


## Main libraries used

- [Roda](https://roda.jeremyevans.net/)
- [Sequel](https://sequel.jeremyevans.net/)


## Environment variables

`DATABASE_URL`


## Author

Gonzalo Mena-Mendoza &lt;gonzalo@mena.com.mx&gt;
